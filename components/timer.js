/*This is an Example of Timer/Stopwatch in React Native */
import React, {Component} from 'react';
//import React in our project

import {Text, View, TouchableHighlight} from 'react-native';
//import all the required components
import Styles from '../css/styles';
import {Stopwatch} from 'react-native-stopwatch-timer';
//importing library to use Stopwatch and Timer

export default class TestApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTimerStart: false,
      isStopwatchStart: false,
      timerDuration: 90000,
      resetTimer: false,
      resetStopwatch: false,
      duration: new Date(),
    };
    this.startStopTimer = this.startStopTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.startStopStopWatch = this.startStopStopWatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }
  startStopTimer() {
    this.setState({
      isTimerStart: !this.state.isTimerStart,
      resetTimer: false,
    });
  }

  resetTimer() {
    this.setState({isTimerStart: false, resetTimer: true});
  }

  startStopStopWatch() {
    this.setState({
      isStopwatchStart: !this.state.isStopwatchStart,
      resetStopwatch: false,
    });
  }

  resetStopwatch() {
    this.setState({isStopwatchStart: false, resetStopwatch: true});
  }

  getFormattedTime = time => {
    console.log(typeof time);
    this.setState({duration: time});
  };



  render() {
    return (
      <View>
        <TouchableHighlight
          onPress={this.startStopStopWatch}
          style={
            !this.state.isStopwatchStart
              ? Styles.options.toucbable
              : Styles.options.toucbableStop
          }
          underlayColor="#a1a61a">
          <Text>{!this.state.isStopwatchStart ? 'START' : 'STOP'}</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const handleTimerComplete = () => alert('Custom Completion Function');
