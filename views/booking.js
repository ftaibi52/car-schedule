import React from 'react';
import {
  Button,
  Text,
  View,
  TextInput,
  Alert,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import Styles from '../css/styles';
import DatePicker from 'react-native-datepicker';
import PushController from '../components/pushNotification';
import PushNotification from 'react-native-push-notification';
import {URL} from '../router/router.js';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      token: '',
      user: '',
      userid: '',
      fecha_recogida: '',
      fecha_entrega: '',
      latitude_ini: '',
      longitude_ini: '',
      fecha_recogida_push: new Date(),
      fecha_entrega_push: new Date(),
    };
  }

  async componentDidMount() {
    try {
      const token = await AsyncStorage.getItem('token');
      if (token !== null) {
        // We have data!!
        this.setState({token: token});
        console.log('From localStorage: ', token);
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }

    try {
      const username = await AsyncStorage.getItem('username', () =>
        console.log('getItem completed'),
      );
      if (username !== null) {
        // We have data!!
        this.setState({user: username});
        console.log('From localStorage: ', username);
      } else {
        console.log('No hay resultado en localStrage username');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }

    try {
      const userid = await AsyncStorage.getItem('userid', () =>
        console.log('getItem completed'),
      );
      if (userid !== null) {
        // We have data!!
        this.setState({userid: userid});
        console.log('From localStorage: ', userid);
      } else {
        console.log('No hay resultado en localStrage userid');
      }
    } catch (error) {
      // Error retrieving data
      console.log('Error getItem localStorage:', error);
    }
  }

  insertSchedule() {
    this.setState({isLoading: true});

    if (
      this.state.cliente === undefined ||
      this.state.motivo === undefined ||
      this.state.fecha_entrega === undefined ||
      this.state.fecha_entrega === undefined
    ) {
      Alert.alert('Rellene todos los campos, por favor.');
      this.setState({isLoading: false});
    } else {
      let data = {
        user_id: this.state.userid,
        date_reserva_ini: this.state.fecha_recogida,
        date_reserva_fin: this.state.fecha_entrega,
        cliente: this.state.cliente,
        motivo: this.state.motivo,
      };
      let url = URL + 'insertCarSchedule';
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest',
        },
      }).then(async res => {
        let response = await res.json();
        console.log(response);
        if (response.success === true) {
          let recogida_date = new Date(
            this.state.fecha_recogida.split(' ')[0].split('-')[0] +
              '-' +
              this.state.fecha_recogida.split(' ')[0].split('-')[1] +
              '-' +
              this.state.fecha_recogida.split(' ')[0].split('-')[2] +
              'T' +
              this.state.fecha_recogida.split(' ')[1].split(':')[0] +
              ':' +
              this.state.fecha_recogida.split(' ')[1].split(':')[1] +
              '+01:00',
          );

          let entrega_date = new Date(
            this.state.fecha_entrega.split(' ')[0].split('-')[0] +
              '-' +
              this.state.fecha_entrega.split(' ')[0].split('-')[1] +
              '-' +
              this.state.fecha_entrega.split(' ')[0].split('-')[2] +
              'T' +
              this.state.fecha_entrega.split(' ')[1].split(':')[0] +
              ':' +
              this.state.fecha_entrega.split(' ')[1].split(':')[1] +
              '+01:00',
          );
          PushNotification.localNotificationSchedule({
            message:
              'Tienes que recoger el coche ahora para ir a ' +
              this.state.cliente, // (required)
            date: recogida_date,
          });

          PushNotification.localNotificationSchedule({
            message: 'Tienes que entregar el coche ahora', // (required)
            date: entrega_date,
          });

          this.setState({isLoading: false});
          Alert.alert('Reserva creada');
        } else {
          Alert.alert('Lo sentimos. El coche ya esta reservado en esa fecha.');

          this.setState({isLoading: false});
        }
      });
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={Styles.styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    } else {
      return (
        <View style={Styles.styles.container}>
          <View style={Styles.styles.form}>
            <Text style={Styles.styles.titulo}>
              Inserte los datos de la reserva:
            </Text>
            <TextInput
              value={this.state.cliente}
              onChangeText={cliente => this.setState({cliente})}
              placeholder={'CLIENTE'}
              style={Styles.styles.input}
            />
            <TextInput
              value={this.state.motivo}
              onChangeText={motivo => this.setState({motivo})}
              placeholder={'MOTIVO'}
              style={Styles.styles.input}
            />

            <DatePicker
              style={{width: 200, margin: 10}}
              date={this.state.fecha_recogida}
              mode="datetime"
              format="YYYY-MM-DD HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              dateIcon="false"
              placeholder="RECOGIDA"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                },
                dateInput: {
                  marginLeft: 36,
                  borderRadius: 50,
                  borderColor: 'black',
                },
              }}
              minuteInterval={10}
              onDateChange={datetime => {
                this.setState({fecha_recogida: datetime});
              }}
            />

            <DatePicker
              style={{width: 200, margin: 10}}
              date={this.state.fecha_entrega}
              mode="datetime"
              format="YYYY-MM-DD HH:mm"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              placeholder="ENTREGA"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0,
                },
                dateInput: {
                  marginLeft: 36,
                  borderRadius: 50,
                  borderColor: 'black',
                },
              }}
              minuteInterval={10}
              onDateChange={datetime => {
                this.setState({fecha_entrega: datetime});
              }}
            />
            <Button
              title={'Reservar'}
              style={Styles.styles.reservar}
              color="#43E580"
              onPress={this.insertSchedule.bind(this)}
            />
            <PushController />
          </View>
        </View>
      );
    }
  }
}
