import React from 'react';
import {View} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Reservar from '../views/booking';
import Inicio from '../views/counter';
import Reservas from '../views/mySchedules';
import Icon from 'react-native-vector-icons/FontAwesome5';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Inicio />
      </View>
    );
  }
}

class ReservarScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Reservar />
      </View>
    );
  }
}

class ReservasScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <Reservas />
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    path: '/',
    navigationOptions: {
      tabBarIcon: ({focused, tintColor}) => {
        return <Icon name="car" size={25} color={tintColor} />;
      },
    },
  },
  Reservar: {
    screen: ReservarScreen,
    path: '/',
    navigationOptions: {
      tabBarIcon: ({focused, tintColor}) => {
        return <Icon name="clock" size={25} color={tintColor} />;
      },
    },
  },
  Reservas: {
    screen: ReservasScreen,
    path: '/',
    navigationOptions: {
      tabBarIcon: ({focused, tintColor}) => {
        return <Icon name="calendar-week" size={25} color={tintColor} />;
      },
    },
  },
});

export default createAppContainer(TabNavigator);
